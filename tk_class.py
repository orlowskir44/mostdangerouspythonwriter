from tkinter import *
import random

COLORS = ['green','red','purple','pink','orange','lime']

class Tkinter:

    def __init__(self):
        self.w_count = None
        self.time = 5
        self.does_write = False
        self.attempt = 1

        self.window = Tk()
        self.window.config(padx=100, pady=45)

        self.text_window = Text(self.window,height=8, width=100, highlightthickness=1 )
        self.text_label = Label(self.window, text="Don't stop.")
        self.timer_label = Label(self.window, text='⏰')

        self.text_label.config(font=("Courier", 14))

        self.text_label.pack()
        self.timer_label.pack()
        self.text_window.pack()

        self.text_window.bind("<Key>", self.check_flag)

        self.window.mainloop()

    def timer(self):

        self.timer_label.config(text=f'Time left: {self.time} ⏰')

        if self.time <= 0:
            x = self.text_window.get("1.0",'end')
            self.text_window.delete("1.0",'end')
            self.time = 5
            self.w_count = Label(text=f"Attempt: {self.attempt}.\tYour timer has ended. You've written {len(x.split())} words.",
                                 bg=random.choice(COLORS))
            self.w_count.pack()

            self.attempt +=1
            self.does_write = False

        else:
            self.window.after(1000,self.timer)

            self.time -= 1

    def check_flag(self, event=None):
        if self.does_write:
            self.reset_timer()
        else:
            self.does_write = True
            self.timer()


    def reset_timer(self):
        self.time = 5
